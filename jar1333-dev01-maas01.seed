## MaaS 설치를 위한 Ubuntu 18.04 Preseed 파일
## 작성자: 김대원 선임
## 작성일: 2020.03.12

## locale은 US로 설정한다.
d-i debian-installer/locale string en_US


## 키보드는 당연히 US로 설정
## 키보드 인지를 위해서 사용자의 input을 요청하는건 하지 않는다.
d-i console-setup/ask_detect boolean false
d-i keyboard-configuration/xkb-keymap select us

### Network configuration
## 자동으로 네트워크 인터페이스를 설정하도록 함
d-i netcfg/choose_interface select auto



## 만약에 수동으로 네트워크를 설정하고 싶다면 아래 항목들을 잘 활용하면 된다.
d-i netcfg/disable_autoconfig boolean true

## Static network configuration.
# IPv4 example
d-i netcfg/get_ipaddress string 192.168.201.3
d-i netcfg/get_netmask string 255.255.255.0
d-i netcfg/get_gateway string 192.168.201.81
d-i netcfg/get_nameservers string 1.1.1.1
d-i netcfg/confirm_static boolean true

# IPv6 example
#d-i netcfg/get_ipaddress string fc00::2
#d-i netcfg/get_netmask string ffff:ffff:ffff:ffff::
#d-i netcfg/get_gateway string fc00::1
#d-i netcfg/get_nameservers string fc00::1
#d-i netcfg/confirm_static boolean true

## hostname 값을 설정한다. DHCP를 사용한다면, DHCP를 통해서 받는 값이 우선권을 가져가서 적용이 되지 않겠지만, Any hostname and domain names assigned from dhcp 
## 그래도 설치시에 질문이 뜨지는 않는 장점이 있다. 
d-i netcfg/get_hostname string maas01
d-i netcfg/get_domain string dev01.jar1333.smsolutions.io



### Mirror settings
# If you select ftp, the mirror/country string does not need to be set.
#d-i mirror/protocol string ftp
d-i mirror/country string manual
d-i mirror/http/hostname string archive.ubuntu.com
d-i mirror/http/directory string /ubuntu
d-i mirror/http/proxy string

# Alternatively: by default, the installer uses CC.archive.ubuntu.com where
# CC is the ISO-3166-2 code for the selected country. You can preseed this
# so that it does so without asking.
#d-i mirror/http/mirror select CC.archive.ubuntu.com

# Suite to install.
#d-i mirror/suite string stretch
# Suite to use for loading installer components (optional).
#d-i mirror/udeb/suite string stretch
# Components to use for loading installer components (optional).
#d-i mirror/udeb/components multiselect main, restricted

## ubuntu에서는 root를 설정하지 않으니, 그냥 일반 사용자로 넘어간다. 
## ubuntu에서는 일반 사용자가 sudo를 사용하는것을 권장한다.

# tokdosirak 일반 사용자 생성. 비밀번호는 hash로 생성
d-i passwd/user-fullname string tokdosirak
d-i passwd/username string tokdosirak
d-i passwd/user-password-crypted password $6$0dfKpqBX7nREYo8H$DueJU309qf4cCfVELf0IkV6ZOVKoIIcKBrtjgPOEGa5ELwcli6l2CUpMXSxrhbksh5LcBH6I5sIt2dA1Eyj02/

## home 디렉토리 암호화는 하지 않습니다.
d-i user-setup/encrypt-home boolean false

### Clock and time zone setup
# Controls whether or not the hardware clock is set to UTC.
d-i clock-setup/utc boolean true

## 타임존은 서울로 설정
d-i time/zone string Asia/Seoul

# Controls whether to use NTP to set the clock during the install
d-i clock-setup/ntp boolean true

## 아직은 NTP가 없지만, 향후 필요시에 작성
#d-i clock-setup/ntp-server string ntp.example.com


## Partition 생성하기
## Partitioning example

d-i partman-auto/method string regular

d-i partman-auto/choose_recipe select atomic
d-i partman/default_filesystem string ext4

# remove any RAID partitioning
d-i partman-md/device_remove_md boolean true

# don't confirm anything
d-i partman-basicfilesystems/no_mount_point boolean false
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true


### Base system installation


### Package selection
tasksel tasksel/first multiselect standard, ssh-server
d-i pkgsel/include string openssh-server

# upgrade all packages
d-i pkgsel/upgrade select full-upgrade

# Language pack selection
d-i pkgsel/language-packs multiselect en



# This is fairly safe to set, it makes grub install automatically to the MBR
# if no other operating system is detected on the machine.
d-i grub-installer/only_debian boolean true

# This one makes grub-installer install to the MBR if it also finds some other
# OS, which is less safe as it might not be able to boot that other OS.
d-i grub-installer/with_other_os boolean true


d-i finish-install/keep-consoles boolean true
d-i finish-install/reboot_in_progress note
